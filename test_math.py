
import math_functions as mf

def test_add():
    assert mf.add(1, 5) == 6

def test_subtract():
    assert mf.substract(5, 8) == -3


def test_multiply():
    assert mf.multiply(10, 10) == 100

def test_division():
    assert mf.division(10, 5) == 2

def test_quotient():
    assert mf.quotient(10, 4) == 2

def test_increment():
    assert mf.increment(5) == 6


def test_decrement_by_2():
    assert mf.decrement_by_2(5) == 3